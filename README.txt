PHP & Web Toolkit

=== IMPORTANT ===

The source code here is not yet ready to be downloaded and compiled.

=== Description ===

This is a plug-in for Panic's Coda IDE.


=== System Requirements ===

- OS X 10.6+
- Coda 1 or Coda 2

=== More information ===

http://www.chipwreck.de/blog/software/coda-php/